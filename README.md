# Testimonial Style

## About

### Author

Ryan E. Anderson

---

### Description

Testimonial Style is a CSS style sheet that can be used to easily create a presentation for a testimonial or a headline.

- Testimonial Style can be used to style a testimonial that is structured with a header, a body, and a footer.
- Testimonial Style includes classes for legacy contexts.

---

### Version

0.1.0

---

### License

Apache-2.0

---

## Using Testimonial Style in a Project

### Installation

The Testimonial Style style sheet can be used in a project by linking to it.

Add the raw style sheet.

```html
<link rel="stylesheet" href="css/testimonial-style.css" type="text/css" />
```

If optimization is necessary, then use the compressed version.

```html
<link rel="stylesheet" href="css/testimonial-style.min.css" type="text/css" />
```

Below is sample markup of an HTML structure utilizing the classes of Testimonial Style to create a simple testimonial 
(See demo.).

```html
<article>
    <div class="text-container">
        <h4 class="golden-line-height">Testimonial</h4>
    </div>
    <div id="testimonial-style-sheet" class="testimonial">
        <div class="testimonial-header">
            <div class="testimonial-text-container">
                <p class="testimonial-text golden-line-height">Ryan says,</p>
            </div>
            <div class="clear-fix"></div>
        </div>
        <div class="testimonial-body">
            <div class="testimonial-quotation-golden">
                <p class="testimonial-quotation-golden-left">&ldquo;</p>
            </div>
            <div class="testimonial-text-container">
                <p class="testimonial-text testimonial-text-medium golden-line-height">
                    This style sheet will help you easily create a testimonial for a layout or a website.
                </p>
            </div>
            <div class="clear-fix"></div>
            <div class="testimonial-quotation-golden">
                <p class="testimonial-quotation-golden-right even-spacing">&ldquo;</p>
            </div>
        </div>
        <div class="testimonial-footer">
            <div class="testimonial-author-container">
                <div class="testimonial-text-container">
                    <p class="testimonial-text testimonial-author golden-line-height">&mdash; Ryan
                        Anderson</p>
                </div>
                <div class="clear-fix"></div>
                <div class="testimonial-text-container">
                    <p class="testimonial-text testimonial-author-information golden-line-height">
                        Creator of Testimonial Style
                    </p>
                </div>
                <div class="clear-fix"></div>
                <div class="testimonial-text-container">
                    <p class="testimonial-text testimonial-author-information golden-line-height">U.S.A.</p>
                </div>
                <div class="clear-fix"></div>
            </div>
        </div>
    </div>
</article>
```

## Remarks

### Spacing

- If extra space appears above a closing quotation mark, then the .even-spacing class can be used to create uniform 
spacing.
- If .even-spacing cannot be used, then adjustments to the margin of an element that contains a closing quotation mark 
can be made until spacing looks uniform (See demo.).

## Demo

Three static HTML files are provided to evaluate the style sheet. To use the demo for modern contexts, deploy all 
contents from the root of this project to a directory on a web server; and make any necessary mappings for virtual 
hosts. To use the demos for legacy contexts, simply open the files in a browser.

### Support

The markup of the demo is supported by legacy browsers; however, newer JavaScript features won't be supported in some 
older contexts because of the modern version of ECMAScript that is used in the implementations of a module and a script.