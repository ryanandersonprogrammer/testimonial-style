/*!
 * Copyright 2020 Ryan E. Anderson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*!
 * Testimonial Style v0.1.0 Demo Module
 *
 * by Ryan E. Anderson
 *
 * Copyright (C) 2020 Ryan E. Anderson
 */
const initializePalettePicker = (tablePaletteRowCollectionSelector, colorPaletteObject, foregroundTargetSelector,
                                 backgroundTargetSelector, tableFootCellOutputTargetSelector,
                                 targetForegroundHorizontalLineRules = true, defaultColorObject = {
        background: "#ffffff",
        foreground: "#000000"
    }) => {
    if (typeof tablePaletteRowCollectionSelector !== "string")
        throw "The selector for the row collection of the table representing a palette must be a string.";

    if (typeof foregroundTargetSelector !== "string")
        throw "The value for the foreground target must be a valid string selector.";

    if (typeof backgroundTargetSelector !== "string")
        throw "The value for the background target must be a valid string selector.";

    if (typeof tableFootCellOutputTargetSelector !== "string")
        throw "The selector for the cell of the table foot must be a string.";

    if (typeof targetForegroundHorizontalLineRules !== "boolean")
        throw "The value of the parameter for determining whether or not the border colors of horizontal rules contained in the foreground should be targeted must be either true or false.";

    if (!Object.prototype.hasOwnProperty.call(defaultColorObject, "background"))
        throw "The default color object must contain a background property.";

    if (!Object.prototype.hasOwnProperty.call(defaultColorObject, "foreground"))
        throw "The default color object must contain a foreground property.";

    const tableRows = document.querySelectorAll(tablePaletteRowCollectionSelector);
    const backgroundRow = [...tableRows][0];
    const foregroundRow = [...tableRows][1];
    const backgroundRowData = backgroundRow.querySelectorAll("td");
    const foregroundRowData = foregroundRow.querySelectorAll("td");
    const foregroundTarget = document.querySelector(foregroundTargetSelector);
    const backgroundTarget = document.querySelector(backgroundTargetSelector);
    const tableFooterCellOutputTarget = document.querySelector(tableFootCellOutputTargetSelector);
    const fixedPaletteWrapper = document.querySelector("#fixed-palette-wrapper");
    const handleMouseLeaveEvent = () => {
        backgroundTarget.style.backgroundColor = defaultColorObject.background;

        foregroundTarget.style.color = defaultColorObject.foreground;

        if (targetForegroundHorizontalLineRules) {
            const horizontalLineRules = foregroundTarget.querySelectorAll("hr");

            [...Array.from(horizontalLineRules)].forEach(entry => entry.style.borderColor =
                defaultColorObject.foreground);
        }
    };
    const handleMouseLeaveEventPaletteWrapper = (event) => {
        const eventTarget = event.target;
        const targetClassList = eventTarget.classList;

        eventTarget.querySelector("#palette").style.display = "none";

        toggleClassList(targetClassList, "hide", "show");

        handleMouseLeaveEvent();
    };
    const handleMouseEnterEventPaletteWrapper = (event) => {
        const eventTarget = event.target;
        const targetClassList = eventTarget.classList;

        eventTarget.querySelector("#palette").style.display = "table";

        toggleClassList(targetClassList, "show", "hide");
    };
    const handleTouchEndEvent = (event) => {
        const eventTarget = event.target;
        const targetClassList = eventTarget.classList;

        let eventToDispatch;

        if (targetClassList.contains("show"))
            eventToDispatch = "mouseleave";
        else
            eventToDispatch = "mouseenter";

        eventTarget.dispatchEvent(new Event(eventToDispatch));
    };
    const toggleClassList = (classList, addClass, removeClass) => {
        if (classList.contains(removeClass)) {
            classList.remove(removeClass);

            classList.add(addClass);
        }
    };

    fixedPaletteWrapper.addEventListener("mouseleave", handleMouseLeaveEventPaletteWrapper);
    fixedPaletteWrapper.addEventListener("touchend", handleTouchEndEvent);
    fixedPaletteWrapper.addEventListener("mouseenter", handleMouseEnterEventPaletteWrapper);

    let i = 0;

    Object.keys(colorPaletteObject).forEach(key => {
        const currentBackgroundRowData = backgroundRowData[i];
        const currentForegroundRowData = foregroundRowData[i];

        currentBackgroundRowData.style.backgroundColor = colorPaletteObject[key].background;

        currentForegroundRowData.style.backgroundColor = colorPaletteObject[key].foreground;

        const handleMouseEnterEvent = () => {
            backgroundTarget.style.backgroundColor = colorPaletteObject[key].background;

            foregroundTarget.style.color = colorPaletteObject[key].foreground;

            tableFooterCellOutputTarget.innerText = "";

            tableFooterCellOutputTarget.appendChild(document.createTextNode(`b/f: \
${colorPaletteObject[key].background}/${colorPaletteObject[key].foreground}`));

            if (targetForegroundHorizontalLineRules) {
                const horizontalLineRules = foregroundTarget.querySelectorAll("hr");

                [...Array.from(horizontalLineRules)].forEach(entry => entry.style.borderColor = colorPaletteObject[key].foreground);
            }
        };

        currentBackgroundRowData.addEventListener("mouseleave", handleMouseLeaveEvent);
        currentBackgroundRowData.addEventListener("mouseenter", handleMouseEnterEvent);

        currentForegroundRowData.addEventListener("mouseleave", handleMouseLeaveEvent);
        currentForegroundRowData.addEventListener("mouseenter", handleMouseEnterEvent);

        i++;
    });
};

export default initializePalettePicker;